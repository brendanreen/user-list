'use strict';

describe('confirmationModalService', function () {

    var modal;
    var result = 'foo';
    var confirmationModalService;

    beforeEach(module('app'));

    beforeEach(function () {
        modal = {
            open: jasmine.createSpy('open').andReturn({
                result: result
            })
        };

        module(function ($provide) {
            $provide.value('$modal', modal);
        });
    });

    beforeEach(inject(function (_confirmationModalService_) {
        confirmationModalService = _confirmationModalService_;
    }));

    it('should return the result of the modal', inject(function ($modal) {
        expect(confirmationModalService.createModal()).toEqual(result);
    }));

});