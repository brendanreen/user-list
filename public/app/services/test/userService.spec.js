'use strict';

describe('userService', function () {

    var httpBackend;
    var userService;

    beforeEach(module('app'));

    beforeEach(inject(function ($httpBackend, _userService_) {
        httpBackend = $httpBackend;
        userService = _userService_;

        httpBackend.when('GET', 'users.json').respond({
            users: []
        });
    }));

    afterEach(function () {
        httpBackend.verifyNoOutstandingExpectation();
        httpBackend.verifyNoOutstandingRequest();
    });

    it('should get the seed users via http if it has not already', function () {
        userService.getUsers().then(function (users) {
            expect(users).toBeDefined();
        });
        httpBackend.flush();
    });

    it('should not allow duplicate email addresses', function () {
        var users;
        var status;

        userService.getUsers().then(function (_users) {
            users = _users;

            var user1 = {
                id: 1,
                lastName: 'foo',
                firstName: 'bar',
                email: 'foo@bar.baz'
            };
            users.push(user1);

            var user2 = angular.copy(user1);
            user2.id = 2;

            userService.editUser(user2).then(angular.noop, function (reason) {
                status = reason.status;
            });
        });

        httpBackend.flush();

        expect(status).toBe(409);
    });

    it('should require last name, first name, and email', function () {
        var statuses = [];

        userService.getUsers().then(function () {

            var user = {
                lastName: 'foo',
                firstName: 'bar',
                email: 'foo@bar.baz'
            };

            var user1 = angular.copy(user);
            delete user1.lastName;

            var user2 = angular.copy(user);
            delete user2.firstName;

            var user3 = angular.copy(user);
            delete user3.email;

            userService.editUser(user1).then(angular.noop, function (reason) {
                statuses[0] = reason.status;
            });

            userService.editUser(user2).then(angular.noop, function (reason) {
                statuses[1] = reason.status;
            });

            userService.editUser(user3).then(angular.noop, function (reason) {
                statuses[2] = reason.status;
            });
        });

        httpBackend.flush();

        expect(statuses).toEqual([412, 412, 412]);
    });

    it('should allow valid edits', function () {
        var editedUser;

        userService.getUsers().then(function () {

            var user = {
                lastName: 'foo',
                firstName: 'bar',
                email: 'foo@bar.baz'
            };

            userService.editUser(user).then(function (user) {
                editedUser = user;
            });
        });

        httpBackend.flush();

        expect(editedUser).toBeDefined();
    });

});