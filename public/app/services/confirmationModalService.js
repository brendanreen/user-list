'use strict';

app.factory('confirmationModalService', ['$modal', function ($modal) {

    return {
        createModal: function (params) {
            var modalInstance = $modal.open({
                templateUrl: 'app/features/confirmation-modal/confirmation-modal.html',
                controller: 'ConfirmationModalCtrl',
                resolve: {
                    message: function () {
                        return params.message
                    }
                }
            });
            return modalInstance.result;
        }
    }

}]);