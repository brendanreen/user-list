'use strict';

app.factory('userService', ['$q', '$http', function ($q, $http) {

    var users;
    var maxId = 0;

    var uniqueEmail = function (users, user) {
        var isUnique = true;
        angular.forEach(users, function (existingUser) {
            if (existingUser.id != user.id && existingUser.email == user.email) {
                isUnique = false;
            }
        });
        return isUnique;
    };

    return {
        getUsers: function () {
            var usersDeferred = $q.defer();

            $http.get('users.json').then(function (response) {
                users = response.data.users;

                angular.forEach(users, function (user) {
                    if (user.id > maxId) {
                        maxId = user.id;
                    }
                });

                usersDeferred.resolve(users);
            });

            return usersDeferred.promise;
        },

        editUser: function (user) {
            var userDeferred = $q.defer();

            if (!uniqueEmail(users, user)){
                userDeferred.reject({ status: 409 });

            } else if (!(user.lastName && user.firstName && user.email)) {
                userDeferred.reject({ status: 412 });

            } else {
                var now = new Date();
                user.id = user.id || ++maxId;
                user.createdOn = user.createdOn || now;
                user.lastEdited = now;
                userDeferred.resolve(user);
            }

            return userDeferred.promise;
        }
    }

}]);