'use strict';

describe('offset', function () {

    var offset;

    beforeEach(module('app'));

    beforeEach(inject(function ($filter) {
        offset = $filter('offset');
    }));

    it('should return the full array at start 0', function () {
        var nums = [0, 1, 2];
        expect(offset(nums, 0)).toEqual(nums);
    });

    it('should return a subset of the array starting at start', function () {
        var nums = [0, 1, 2];
        expect(offset(nums, 2)).toEqual([2]);
    });

    it('should gracefully handle bogus parameters', function () {
        var nums;
        expect(offset(nums, 0)).toBeUndefined();

        nums = [];
        expect(offset(nums, 0)).toEqual([]);

        nums = [0];
        expect(offset(nums, 1)).toEqual([]);
    });

});