app.filter('offset', function () {
    return function (input, start) {
        if (input) return input.slice(start);
    };
});