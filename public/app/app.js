'use strict';

var app = angular.module('app', ['ngRoute', 'ui.bootstrap', 'userList', 'confirmationModal']);

app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/blank', {
        template: '<div class="jumbotron"><a href="/">User List</a></div>'

    }).otherwise({
        redirectTo: 'user-list'
    });
}]);