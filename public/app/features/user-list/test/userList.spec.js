'use strict';

describe('UserListCtrl', function () {

    var scope;
    var userService;
    var confirmationModalService;
    var userListCtrl;

    beforeEach(module('app'));

    beforeEach(inject(function ($q) {
        userService = {
            getUsers: function () {
                var usersDeferred = $q.defer();
                usersDeferred.resolve([
                    {
                        firstName: 'Foo',
                        lastName: 'Bar',
                        email: 'foo@bar.baz',
                        active: false,
                        isEditing: false
                    }
                ]);
                return usersDeferred.promise;
            },

            editUser: function (user) {
                var userDeferred = $q.defer();
                user.lastEdited = new Date();

                if (user._error) {
                    userDeferred.reject({ status: user._error })

                } else {
                    userDeferred.resolve(user);
                }

                return userDeferred.promise;
            }
        };
    }));

    beforeEach(function () {
        confirmationModalService = {
            createModal: angular.noop
        };
        spyOn(confirmationModalService, 'createModal');
    });

    beforeEach(inject(function ($rootScope, $controller, $compile) {
        scope = $rootScope.$new();
        var userListForm = angular.element(
                '<form name="userListForm">' +
                '<input type="text" name="lastName" data-ng-model="user.lastName" data-ng-change="fieldChanged(user)"/>' +
                '</form>');
        $compile(userListForm)(scope);

        userListCtrl = $controller('UserListCtrl', {
            $scope: scope,
            userService: userService
        });

        spyOn(scope, 'fieldChanged').andCallThrough();
    }));

    it('should retrieve a list of users', function () {
        scope.$apply();
        expect(scope.users).toBeDefined();
    });

    it('should allow addition of a new editable user', function () {
        scope.$apply();
        scope.users = [];

        var numUsers = scope.users.length;
        scope.addUser();

        expect(scope.users.length).toBeGreaterThan(numUsers);
        expect(scope.users[0]).toBeDefined();
        expect(scope.users[0].isEditing).toBe(true);
    });

    it('should allow edit of existing user and maintain a copy of the original', function () {
        scope.$apply();
        var original = angular.copy(scope.users[0]);

        scope.startEdit(scope.users[0]);
        scope.users[0].lastName = 'Doe';

        expect(scope.users[0]._original).toEqual(original);
        expect(scope.users[0].isEditing).toBe(true);
    });

    it('should revert changes if a user edit is cancelled', function () {
        scope.$apply();
        var original = angular.copy(scope.users[0]);

        scope.startEdit(scope.users[0]);
        scope.users[0].lastName = 'Doe';

        scope.cancelEdit(scope.users[0]);

        expect(scope.users[0]).toEqual(original);
        expect(scope.users[0].isEditing).toBe(false);
    });

    it('should remove a new row if it is cancelled', function () {
        scope.$apply();
        scope.users = [];

        var numUsers = scope.users.length;
        scope.addUser();
        scope.cancelEdit(scope.users[0]);
        scope.$apply();

        expect(scope.users.length).toEqual(numUsers);
    });

    it('should save a valid edit', function () {
        scope.$apply();
        var original = angular.copy(scope.users[0]);

        scope.startEdit(scope.users[0]);
        scope.users[0].lastName = 'Doe';

        scope.saveEdit(scope.users[0]);

        expect(scope.users[0]).not.toEqual(original);
        scope.$apply();
        expect(scope.users[0].isEditing).toBe(false);
    });

    it('should apply errors on failed save', function () {
        scope.$apply();

        scope.users[0]._error = 409;
        scope.saveEdit(scope.users[0]);
        scope.$apply();
        expect(scope.users[0].error.email).toBe(true);

        scope.users[0]._error = 412;
        delete scope.users[0].lastName;
        scope.saveEdit(scope.users[0]);
        scope.$apply();
        expect(scope.users[0].error.lastName).toBe(true);
    });

    it('should change sorting and default to ascending order when new predicate is selected', function () {
        scope.orderBy('foo');
        expect(scope.predicate).toEqual('+foo');

        scope.orderBy('bar');
        expect(scope.predicate).toEqual('+bar');
    });

    it('should reverse order when same predicate is selected again', function () {
        scope.orderBy('foo');
        expect(scope.predicate).toEqual('+foo');

        scope.orderBy('foo');
        expect(scope.predicate).toEqual('-foo');

        scope.orderBy('foo');
        expect(scope.predicate).toEqual('+foo');
    });

    it('should increment page if next page is available', function () {
        scope.$apply();
        var user = scope.users[0];
        while (scope.users.length <= scope.pageSize) {
            scope.users.push(angular.copy(user));
        }
        scope.filteredUsers = scope.users;

        var page = scope.page;
        scope.nextPage();
        expect(scope.page).toBeGreaterThan(page);
    });

    it('should not increment page if on last page', function () {
        scope.$apply();
        var user = scope.users[0];
        while (scope.users.length < scope.pageSize) {
            scope.users.push(angular.copy(user));
        }
        scope.filteredUsers = scope.users;

        var page = scope.page;
        scope.nextPage();
        expect(scope.page).toEqual(page);
    });

    it('should decrement page if previous page is available', function () {
        scope.$apply();
        var user = scope.users[0];
        while (scope.users.length <= scope.pageSize) {
            scope.users.push(angular.copy(user));
        }
        scope.filteredUsers = scope.users;

        scope.nextPage();
        var page = scope.page;

        scope.prevPage();
        expect(scope.page).toBeLessThan(page);
    });

    it('should not decrement page if on first page', function () {
        scope.$apply();
        var user = scope.users[0];
        while (scope.users.length < scope.pageSize) {
            scope.users.push(angular.copy(user));
        }
        scope.filteredUsers = scope.users;

        scope.prevPage();
        expect(scope.page).toBe(0);
    });

    it('should determine total number of pages if users have been loaded and filtered', function () {
        scope.$apply();
        var user = scope.users[0];
        while (scope.users.length < scope.pageSize) {
            scope.users.push(angular.copy(user));
        }
        scope.filteredUsers = scope.users;

        expect(scope.totalPages()).toBe(0);

        scope.users.push(angular.copy(user));
        expect(scope.totalPages()).toBe(1);
    });

    it('should have 0 pages if users have not been loaded and filtered', function () {
        expect(scope.totalPages()).toBe(0);
    });

    it('should toggle active state of user', function () {
        scope.$apply();
        var user = scope.users[0];
        var active = user.active;

        scope.toggleActive(user);
        expect(user.active).not.toEqual(active);

        scope.toggleActive(user);
        expect(user.active).toEqual(active);
    });

    it('should clear validation when validated field text changes', function () {
        scope.userListForm.lastName.$setViewValue('foo');
        expect(scope.fieldChanged).toHaveBeenCalledWith(scope.user);
    });

    it('should return whether or not any users have unsaved changes', function () {
        scope.$apply();
        expect(scope.unsavedChanges()).toBe(false);

        scope.startEdit(scope.users[0]);
        expect(scope.unsavedChanges()).toBe(true);
    });

    it('should not block route change if there is not unsaved data', inject(function ($location) {
        scope.$apply();
        $location.path('/foo');
        expect($location.path()).toEqual('/foo');
    }));

});