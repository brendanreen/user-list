'use strict';

var userList = angular.module('userList', []);

userList.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/user-list', {
        templateUrl: 'app/features/user-list/user-list.html',
        controller: 'UserListCtrl'
    });
}]);

userList.controller('UserListCtrl', ['$scope', '$rootScope', '$window', '$q', 'userService', 'confirmationModalService',
    function ($scope, $rootScope, $window, $q, userService, confirmationModalService) {

        var clearValidation = function (user) {
            $scope.userListForm.$setValidity('uniqueEmail', true);
            $scope.userListForm.$setValidity('required', true);
            delete user.error;
        };

        $scope.addUser = function () {
            $scope.users.unshift({
                isEditing: true,
                isNew: true
            });
        };

        $scope.startEdit = function (user) {
            user._original = angular.copy(user);
            user.isEditing = true;
        };

        $scope.cancelEdit = function (user) {
            if (user.isNew) {
                $scope.remove(user);

            } else {
                angular.copy(user._original, user);
                user.isEditing = false;
            }

            clearValidation(user);
        };

        $scope.saveEdit = function (user) {
            userService.editUser(user).then(function (editedUser) {
                angular.extend(editedUser, user);
                user.isEditing = false;
                delete user.isNew;

            }, function (reason) {
                switch (reason.status) {
                    case 409:
                        $scope.userListForm.$setValidity('uniqueEmail', false);
                        user.error = user.error || {};
                        user.error.email = true;
                        break;

                    case 412:
                        $scope.userListForm.$setValidity('required', false);
                        user.error = user.error || {};
                        if (!user.lastName) user.error.lastName = true;
                        if (!user.firstName) user.error.firstName = true;
                        if (!user.email) user.error.email = true;
                        break;
                }
            });
        };

        $scope.toggleActive = function (user) {
            user.active = !user.active;
        };

        $scope.remove = function (user) {
            var confirmationDeferred = $q.defer();
            if (user.isNew) {
                confirmationDeferred.resolve();

            } else {
                confirmationModalService.createModal({
                    message: 'Are you sure you want to delete ' + user.lastName + ', ' + user.firstName + '?'

                }).then(function () {
                    confirmationDeferred.resolve();

                }, function () {
                    confirmationDeferred.reject();
                })
            }

            confirmationDeferred.promise.then(function () {
                var index = $scope.users.indexOf(user);
                if (index > -1) {
                    $scope.users.splice(index, 1);
                }
            });
        };

        $scope.orderBy = function (predicate) {
            if ($scope.predicate.substring(1) == predicate) {
                if ($scope.predicate.indexOf('-') > -1) {
                    $scope.predicate = '+' + predicate;

                } else {
                    $scope.predicate = '-' + predicate;
                }

            } else {
                $scope.predicate = '+' + predicate;
            }
        };

        $scope.nextPage = function () {
            if ($scope.page < $scope.totalPages()) {
                $scope.page++;
            }
        };

        $scope.prevPage = function () {
            if ($scope.page > 0) {
                $scope.page--;
            }
        };

        $scope.totalPages = function () {
            if ($scope.filteredUsers && $scope.pageSize) {
                return Math.ceil($scope.filteredUsers.length / $scope.pageSize) - 1;

            } else {
                return 0;
            }
        };

        $scope.unsavedChanges = function () {
            var unsavedChanges = false;
            angular.forEach($scope.users, function (user) {
                if (user.isEditing) {
                    unsavedChanges = true;
                }
            });
            return unsavedChanges;
        };

        $scope.fieldChanged = function (user) {
            clearValidation(user);
        };

        var confirmUnsaved = function (event, url) {
            if ($scope.unsavedChanges()) {
                event.preventDefault();

                confirmationModalService.createModal({
                    message: 'You have unsaved changes. Are you sure you wish to discard them?'

                }).then(function () {
                    toggleRouteBlock();
                    $window.location.href = url;
                });
            }
        };

        var toggleRouteBlock = $rootScope.$on('$locationChangeStart', confirmUnsaved);

        $scope.predicate = '+lastName';
        $scope.reverse = false;
        $scope.pageSize = 10;
        $scope.page = 0;

        userService.getUsers().then(function (users) {
            $scope.users = users;
        });

    }
]);