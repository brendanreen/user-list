'use strict';

var confirmationModal = angular.module('confirmationModal', []);

confirmationModal.controller('ConfirmationModalCtrl', ['$scope', '$modalInstance', 'message',
    function ($scope, $modalInstance, message) {

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.confirm = function () {
            $modalInstance.close();
        };

        $scope.message = message;

    }]);