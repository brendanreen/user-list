'use strict';

describe('ConfirmationModalCtrl', function () {

    var scope;
    var modalInstance;
    var message = 'foo';
    var confirmationModalCtrl;

    beforeEach(module('app'));

    beforeEach(function () {
        modalInstance = {
            dismiss: angular.noop,
            close:  angular.noop
        };

        spyOn(modalInstance, 'dismiss');
        spyOn(modalInstance, 'close');
    });

    beforeEach(inject(function ($rootScope, $controller) {
        scope = $rootScope.$new();
        confirmationModalCtrl = $controller('ConfirmationModalCtrl', {
            $scope: scope,
            $modalInstance: modalInstance,
            message: message
        });
    }));

    it('should set the provided message in scope', function () {
        expect(scope.message).toEqual(message);
    });

    it('should dismiss the modal when cancel is called', function () {
        scope.cancel();
        expect(modalInstance.dismiss).toHaveBeenCalledWith('cancel');
    });

    it('should close the modal when confirm is called', function () {
        scope.confirm();
        expect(modalInstance.close).toHaveBeenCalled();
    });

});